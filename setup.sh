#!/usr/bin/env bash
#
# bash script to run setup
#

# possible setup options
OPTION_SYSPREP="sysprep"
OPTION_CONDA_CLEAN="conda_clean"
OPTION_CONDA_SETUP="conda_setup"
OPTION_DOCKER="docker"

RUN_SYSPREP=false
RUN_CONDA_CLEAN=false
RUN_CONDA_SETUP=false
IN_DOCKER=false


# check for positional parameters
if [ "$1" == "" ]; then
    # if no parameters are given we do the full setup without cleaning, but not in docker
    RUN_SYSPREP=true
    RUN_CONDA_SETUP=true
else
    # if there are parameters given we loop through them
    while [ "$1" != "" ]; do
        case "$1" in
            # check if the given parameters fit to a run option
            $OPTION_SYSPREP) RUN_SYSPREP=true;;
            $OPTION_CONDA_CLEAN) RUN_CONDA_CLEAN=true;;
            $OPTION_CONDA_SETUP) RUN_CONDA_SETUP=true;;
            $OPTION_DOCKER) IN_DOCKER=true;;
            *) echo "$1 is unknown option. Allowed options: $OPTION_SYSPREP, $OPTION_CONDA_CLEAN, $OPTION_CONDA_SETUP, $OPTION_DOCKER"
        esac
        # Shift all the parameters down by one
        shift
    done
fi


# get settings
source utils/settings.sh

if $RUN_SYSPREP; then
    # Run system preparation
    bash utils/sysprep.sh $INSTALL_DIR $IN_DOCKER
fi

if $RUN_CONDA_CLEAN; then
    # Run system clean up
    bash utils/clean_conda.sh $CONDA_DIR_OLD
fi

if $RUN_CONDA_SETUP; then
    # Run conda installation
    bash utils/setup_conda.sh $CONDA_DIR_NEW $CONDA_LINK
fi
