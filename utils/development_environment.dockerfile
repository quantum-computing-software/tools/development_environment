FROM ubuntu:20.04

# change workdir to root
WORKDIR /root/development_environment

# copy utils 
COPY utils utils
COPY setup.sh setup.sh

# prepare the system with basic dependencies
RUN bash setup.sh sysprep docker

# install conda
RUN bash setup.sh conda_setup
