#!/usr/bin/env bash
#
# bash script to clean up conda
#

# parameters
CONDA_DIR=$1

# remove conda
rm -rf $CONDA_DIR
if cat ~/.profile | grep ^conda; then
	sed -i '/conda/d' ~/.profile
fi
