#!/usr/bin/env bash
#
# bash script to install missing dependencies
#

# parameters
INSTALL_DIR=$1
IN_DOCKER=$2

function main {
    #
    # main function
    #
    if $IN_DOCKER; then
        sysprep_docker
    elif cat /etc/*release | grep ^NAME | grep Ubuntu; then
        sysprep_ubuntu
    else
        echo "Could not identify the system"
    fi
}

function sysprep_ubuntu {
    #
    # preparation for ubuntu
    #
    sudo apt update -y && apt-get upgrade -y
    sudo apt install -y $(grep -vE "^\s*#" utils/packages.txt | tr "\n" " ")
}

function sysprep_docker {
    #
    # preparation for docker container
    #
    apt-get update -y && apt-get upgrade -y
    export TZ=Europe/Berlin
    ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
    export DEBIAN_FRONTEND=noninteractive
    apt-get install -y $(grep -vE "^\s*#" utils/packages.txt | tr "\n" " ")
}

#
# run main function
# 
main
