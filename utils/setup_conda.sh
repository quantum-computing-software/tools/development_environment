#!/usr/bin/env bash
#
# bash script to install and update conda from miniforge
#

# parameters
CONDA_DIR=$1
CONDA_LINK=$2


# create folder
[ -d deps/ ] || ( mkdir deps/ )

# get wget
apt-get update -y
apt-get install -y wget

# download miniforge installer
wget -O deps/Miniforge3.sh $CONDA_LINK

if [ ! -f deps/Miniforge3* ]
then
    echo "Did not find miniforge setup, probably download failed"
    exit 1
else
    # run sh installer
    bash deps/Miniforge3.sh -b -p $CONDA_DIR
    # write to bashrc
    echo " " >> ~/.profile
    echo "# load miniforge" >> ~/.profile
    echo "source $CONDA_DIR/etc/profile.d/conda.sh" >> ~/.profile
    # activate bashrc
    source ~/.profile
    # update conda
    conda update --all -y
    # activate env
    conda activate base
    conda install conda-build python=3.10
fi
