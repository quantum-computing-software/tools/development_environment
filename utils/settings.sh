#
# Variables
#

# miniforge
CONDA_DIR_OLD="$INSTALL_DIR/miniforge3"
CONDA_DIR_NEW=$CONDA_DIR_OLD
CONDA_LINK="https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-Linux-x86_64.sh"
