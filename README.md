Development Environment for DLR-SC Quantum Computing Projects
=============================================================

[![pipeline status](https://gitlab.com/quantum-computing-software/tools/development_environment/badges/main/pipeline.svg)](https://gitlab.com/quantum-computing-software/tools/development_environment/-/commits/main)

Description
-----------
This package has two purposes:

 - Build a controlled development environment for the DLR-SC quantum computing group projects
 - Build the same environment in a docker container for the DLR-SC quantum computing group continuous integration infrastructure
 

Usage
-----

### Preparation 

The installation is divided into different phases which can be selected from at the top of the script `setup.sh` 

 - `SYSPREP`: Install necessary dependencies (sudo rights necessary)
 - `SYSCLEAN`: Wipe out conda installation (BE CAREFUL: Consider a backup of your conda environments, because they will be wiped out otherwise!)
 - `CONDA`: Install miniforge 

The install directories can also be controlled via environment variables

 - `CONDA_DIR_OLD`

in `setup.sh`

### Installation

Run

```
bash setup.sh
```


### Remarks for Linux VM under Windows host:

1. Clone the repository
2. If cloned under windows, make sure in Linux VM to revert the changed line endings
3. In Linux VM copy the repository folder somewhere outside of shared folder
4. Execute `bash setup.sh` in this folder
